# mean-stack-tutorial FlapperNews modified to be run as a Docker container
https://thinkster.io/mean-stack-tutorial/

# Modifications
For development:
mongoose.connect('mongodb://mongo:27017');

For OpenShift deployment: 
mongoose.connect(process.env.MONGO_URL);

# Build the container
docker-compose build

# Run the container
docker-compose up
