# This image will be based on the official nodejs docker image
FROM node:4.2

RUN mkdir app
# Add all our code inside that directory that lives in the container
COPY . app
# Commands will run in this directory
WORKDIR app

# Install dependencies and generate production dist
RUN npm install

# Tell Docker we are going to use this port
ENV PORT 8080
EXPOSE 8080

# The command to run our app when the container is run
CMD ["npm", "run", "start"]
